# multi stage build
FROM node:14-alpine

WORKDIR /frontend

COPY package*.json ./

RUN npm install

COPY . ./

Run npm run build

# stage two

FROM nginx:latest

COPY default.conf /etc/nginx/conf.d/

COPY --from=0 /frontend/build /usr/share/nginx/html/



